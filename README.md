# Continuous Control - Reacher x20 Environment

## Challenge Details

This project trains an actor-critic method on the [Reacher](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Learning-Environment-Examples.md#reacher) environment using Deep Deterministic Policy Gradients (DDPG) from [Continuous Control with Deep Reinforcement Learning](https://arxiv.org/abs/1509.02971).

![Reacher x20](reacher.gif)

In this environment, a double-jointed arm can move to target locations. A reward of +0.1 is provided for each step that the agent's hand is in the goal location. Thus, the goal of your agent is to maintain its position at the target location for as many time steps as possible.

The observation space consists of 33 variables corresponding to position, rotation, velocity, and angular velocities of the arm. Each action is a vector with four numbers, corresponding to torque applicable to two joints. Every entry in the action vector should be a number between -1 and 1.

This environment contains 20 identical agents, each with its own copy of the environment.

### Solution Case

To solve this environment, the agents must get an average score of +30 over 100 consecutive episodes, and over all agents.  Specifically,
- After each episode, we add up the rewards that each agent received (without discounting), to get a score for each agent.  This yields 20 (potentially different) scores.  We then take the average of these 20 scores.
- This yields an **average score** for each episode (where the average is over all 20 agents).

The environment is considered solved, when the average (over 100 episodes) of those average scores is at least +30.

## Getting Started
This project assumes the Unity environment is installed in a local directory. The filepath in the .ipynb needs to be updated to reflect your filepath. Before starting the notebook, install required python packages by running:

    pip install .

## Instructions
The specific instructions for configuring and running this project are inline within _**Report.ipynb**_, as are the detailed results of the project. In short, the code-base was originally meant to reuse the prior work on DQN, changing the algorithm to DDPG. Due to some incongruities that prevented the code from compiling, much of the codebase was rebased and enhanced for Actor-Critic, leading to a new set of files based based on the materials from Part 3 of DRLND.

## Results
After cleaning the code, it managed to converge on the first try using an actor-critic, clipping the critic and learning at the end of every episode. The system reached a solution incredibly fast; after only 10 episodes (110-100). Details are in _**Report.ipynb**_.
