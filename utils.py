#!/usr/bin/env python3
#==============================================================================
""" Functions for loading and manipulating data/files/directories """
from __future__ import print_function, division, absolute_import
__author__ = 'Henry Leopold'

# ==============================================================================
# Imports
# ------------------------------------------------------------------------------
import os, sys, re
import numpy as np
try: import cPickle as pickle
except ImportError: import pickle
from six.moves import xrange, range, zip
import glob, shelve
import shutil

import pandas as pd
import matplotlib.collections as mc
from IPython import display
from JSAnimation.IPython_display import display_animation
from matplotlib import animation
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle

# ==============================================================================
# Contents
# ------------------------------------------------------------------------------
__all__ = [
        # ===================
        # Read/Write
        # -------------------
          # Pickle Archives
          # -----------------
            'save_pickle',
            'load_pickle',
            'save_config',
        # ===================
        # Filesystem
        # -------------------
          # Directories/paths
          # -----------------
            'build_path',
            'rm_dir',
            'make_dir',
        # ===================
        # Visualization
        # -------------------
          # Score plotting
          # -----------------
            'plot_scores',
            'plot_rolling_scores',
         ]

# ==============================================================================
# Read/Save/write functions
# ------------------------------------------------------------------------------
# Pickle Archives
# -----------------------------------------------------------------------
def save_pickle(data, filepath):
    """Takes in a dataset and pickles it.
    Args:
        data: Data to save
        filepath (str): Path including filename not ending with .pickle
    """
    make_dir(filepath.rsplit('/',1)[0]+'/')
    with open(filepath+'.pickle', 'wb') as f:
        pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)

def load_pickle(filepath):
    """Loads a pickle and returns it as data.
    Args:
        filepath (str): Path including filename, ending with .pickle
    Returns:
        data: Data in format stored in pickle.
    """
    with open(filepath, 'rb') as f:
        data = pickle.load(f)
    return data

def save_config(config, filepath):
    data = {}
    for k,v in config.items():
        if any(type(v) is x for x in [int,float,list,str]):
            data[k] = v
        else:
            data[k] = str(v)
    save_pickle(data, filepath)

# ==============================================================================
# Filesystem Functions
# ==============================================================================
# Directories and paths
# -----------------------------------------------------------------------
def build_path(paths, prefix=''):
    """ Converts a list of directory paths into a single string.
    Args:
        paths (list of str): Each entry is a seperate path.
        prefix (str): Prefix for starting the concatenation.
    """
    filename = prefix
    for path in paths:
        filename = os.path.join(filename,path)
    return filename

def make_dir(dirname="untitled"):
    """Creates a directory if one doesnt exist; to be made into a wrapper...
    Args:
        dirname (str): An empty directory name to create
    """
    if not os.path.exists(dirname):
        os.makedirs(dirname)
        print('Directory created: %s'%dirname)

def rm_dir(dirname):
    """Create an empty directory OR cleans out an existing one
    Args:
        dirname (str): An empty directory name to create
    """
    if os.path.exists(dirname):
        shutil.rmtree(dirname)
    assert os.path.exists(dirname) is False
    return make_dir(dirname)

# ==============================================================================
# Visualization
# ------------------------------------------------------------------------------
# Score plotting
# ------------------------------------------------------------------------------
def plot_scores(scores, savepath=None, transparent=False):
    """Plots the scores from training using matplotlib.
    Args
        scores (list): Numerical list of scores (rewards) during training.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.plot(np.arange(len(scores)), scores)
    plt.ylabel('Score')
    plt.xlabel('Episode #')
    plt.show()
    if savepath:
        plt.savefig(savepath, transparent=transparent)

def plot_rolling_scores(scores, rolling_window=100, savepath=None, transparent=False):
    """Plot scores and optional rolling mean using specified window.
    Args
        scores (list): List of episode scores (usually floats)
        rolling_window (int): Window size
    """
    plt.plot(scores); plt.title("Scores");
    rolling_mean = pd.Series(scores).rolling(rolling_window).mean()
    plt.plot(rolling_mean);
    if savepath:
        plt.savefig(savepath, transparent=transparent)
